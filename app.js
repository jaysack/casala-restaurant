// Viewport adjustment
let height = window.innerHeight;

/* window.addEventListener('resize', adjustViewport);

function adjustViewport() {
    window.innerHeight
} */

// Select Elements

/** 
 * Choice of 'let' (parent divs) and 'const' (children divs) here for readability purposes only
 */

let heading = document.querySelector('.heading');

let socialMediaDiv = document.querySelector('.social-media');
const facebook = document.querySelector('#facebook');
const instagram = document.querySelector('#instagram');
const twitter = document.querySelector('#twitter');

let form = document.querySelector('.form-area');

let illustrations = document.querySelectorAll('.wrapper');
const chicken = document.querySelector('#chicken');
const link = document.querySelector('#link');
const pizza = document.querySelector('#pizza');
const noodles = document.querySelector('#noodles');

// Manipulting Elements
function showLowerElements() {
    heading.classList.toggle('move-to-top');
    form.classList.toggle('move-to-bottom');

    illustrations.forEach((item) => {
        item.classList.toggle('move-to-bottom');
    });
}

$('.arrow').on('click touch', function(e) {

    e.preventDefault();

    let arrow = $(this);
    if(!arrow.hasClass('animate')) {
        arrow.addClass('animate');
        setTimeout(() => {
            arrow.removeClass('animate');
        }, 1600);
    }

    showLowerElements();
    
    $('.arrow-btn').toggleClass('arrowRotate');

});